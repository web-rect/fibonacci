

const $modalMenu = document.querySelector('.modal-menu'),
$menuGamb = document.querySelectorAll('.menu-gamb'),
$header = document.querySelector('.header'),
$fileP = document.querySelector('.file-p p'),
$filePImg = document.querySelector('.file-p img'),
$fileItemInput = document.querySelector('.file-item input'),
$modalVopros = document.querySelector('.modal-vopros'),
$modalVoprosBG = document.querySelector('.modal-vopros .bg'),
$modalVoprosClose = document.querySelector('.modal-vopros .content .close'),
$modalVoprosContent = document.querySelector('.modal-vopros .content'),
$modalVoprosSuccessfull = document.querySelector('.modal-vopros .successfull'),
$modalZvonok = document.querySelector('.modal-zvonok'),
$modalZvonokBG = document.querySelector('.modal-zvonok .bg'),
$modalZvonokClose = document.querySelector('.modal-zvonok .content .close'),
$modalZvonokContent = document.querySelector('.modal-zvonok .content'),
$modalZvonokSuccessfull = document.querySelector('.modal-zvonok .successfull'),
$modalPresent = document.querySelector('.modal-present'),
$modalPresentBG = document.querySelector('.modal-present .bg'),
$modalPresentClose = document.querySelector('.modal-present .content .close'),
$modalPresentContent = document.querySelector('.modal-present .content'),
$modalPresentSuccessfull = document.querySelector('.modal-present .successfull'),
$btn = document.querySelectorAll('.btn'),
$productBoxSlider = document.querySelector('.product-box-slider'),
$topPagination = document.querySelectorAll('.top-pagination p'),
$imgPerfomance = document.querySelectorAll('.img-perfomance'),
$paginationA = document.querySelectorAll('.pagination p'),
$slidersJs = document.querySelector('.sliders-js'),
$slidersJsItems = document.querySelectorAll('.sliders-js .swiper-container .swiper-wrapper .swiper-slide'),
$slidersBodyJs = document.querySelectorAll('.sliders-body-js'),
$imgParallax1 = document.querySelector('.img-parallax-1'),
$imgParallax2 = document.querySelector('.img-parallax-2'),
$gearJs = document.querySelectorAll('.gear-js'),
$menusModalJs = document.querySelector('.menus-modal-js'),
$infoModalJs = document.querySelector('.info-modal-js'),
$linePerf = document.querySelector('.line-perf'),
sectionDwn = document.querySelector('.section-dwn-scroll'),
$virusJs = document.querySelector('.virus-js'),
$borderLike = document.querySelector('.border-like'),
$rowBlocksFlexJs = document.querySelector('.row-blocks-flex-js'),
$blockAdvantages = document.querySelector('.block-advantages'),
$rowImgsJs = document.querySelector('.row-imgs-js'),
$playJsVideo = document.querySelector('.play-js-video'),
$vieoStart = document.querySelector(".vieo-start"),
$nataBg1 = document.querySelector('.nata-bg-1'),
$nataBg2 = document.querySelector('.nata-bg-2'),
$nataImg = document.querySelector('.nata-img'),
$contentBottomJsKr = document.querySelector('.content-bottom-js-kr');


$(function () {
    $("a[href^='#']").click(function () {
        var _href = $(this).attr("href");
        $("html, body").animate({scrollTop: $(_href).offset().top -80 + "px"}, 1500);
        return false;
    });
});

if ($menuGamb) {
    $menuGamb.forEach(function(items){
        items.addEventListener('click', (e) => {
            if (!$modalMenu.classList.contains('active')) {
                $modalMenu.classList.add('active');
                $modalMenu.setAttribute('active', 'active');
                setTimeout(() => {
                    $infoModalJs.classList.add('active');
                }, 500);
            } else {
                setTimeout(() => {
                    $modalMenu.removeAttribute('active');
                    $modalMenu.classList.remove('active');
                    
                }, 500);
                $infoModalJs.classList.remove('active');
            }
        });
    });
}


window.addEventListener('scroll', () => {
    const scrl = window.scrollY;
    if (scrl >= 10) {
        $header.classList.add('active');
    }
    if (scrl <= 10) {
        $header.classList.remove('active');
    }
});

if ($btn) {
    $btn.forEach(function(item){
        item.addEventListener('click', function(e) {
            // e.preventDefault();
            if (this.dataset.btnVopros) {
                $modalVopros.classList.add('active');
                $modalVoprosClose.addEventListener('click', () => {
                    $modalVopros.classList.remove('active');
                });
                $modalVoprosBG.addEventListener('click', () => {
                    $modalVopros.classList.remove('active');
                });
            }
            
            if (this.dataset.btnZayavka) {
                $modalZvonok.classList.add('active');
                $modalZvonokClose.addEventListener('click', () => {
                    $modalZvonok.classList.remove('active');
                });
                $modalZvonokBG.addEventListener('click', () => {
                    $modalZvonok.classList.remove('active');
                });
            }
            
            if (this.dataset.btnPresent) {
                $modalPresent.classList.add('active');
                $modalPresentClose.addEventListener('click', () => {
                    $modalPresent.classList.remove('active');
                });
                $modalPresentBG.addEventListener('click', () => {
                    $modalPresent.classList.remove('active');
                });
            }
            
        });
    });
}


// форма файла
if ($fileItemInput) {
    $fileItemInput.addEventListener('change' , function() {
    
        if (this.files && this.files[0]) {
            let $nameFile = this.files[0].name;
            let $read = new FileReader();
    
            $read.onload = function(e) {
                
                $fileP.textContent = $nameFile;
                
            }
            $read.readAsDataURL(this.files[0]);
        }
    });
}


// slider
let ntstart = false;

if ($productBoxSlider) {

    document.querySelectorAll('.btn-act-3').forEach(item => {
        item.addEventListener('click', (e) => {
            // e.preventDefault();
            ntstart = true;

        });
    });

    $productBoxSlider.addEventListener('click', function(e) {
        // e.preventDefault();
        const $target = e.target;
        let $paginationTop = $target.closest('.top-pagination p');
        let $paginationDown = $target.closest('.pagination p');
        if ($paginationTop) {
            $topPagination.forEach(function(item){
                item.classList.remove('active');
            });
          
            $imgPerfomance.forEach(function(tab) {
                tab.classList.remove('active');
                $paginationA.forEach(function(tabdwn){
                   
                    if ($paginationTop.dataset.numSlider === tab.dataset.numSlider) {
                        tab.classList.add('active');
                        $paginationTop.classList.add('active');
                        tabdwn.classList.remove('active');
                        if (tab.dataset.numSlider === tabdwn.dataset.numSlider) {
                            tabdwn.classList.add('active');
                            
                        }
                    }
                   
                });
    
                
            });
        }
        if ($paginationDown) {
            $paginationA.forEach(function(item){
                item.classList.remove('active');
            });
          
            $imgPerfomance.forEach(function(tab) {
                tab.classList.remove('active');
                $topPagination.forEach(function(tabTop) {
                    if ($paginationDown.dataset.numSlider === tab.dataset.numSlider) {
                        tab.classList.add('active');
                        $paginationDown.classList.add('active');
                        tabTop.classList.remove('active');
                        if (tabTop.dataset.numSlider === tab.dataset.numSlider) {
                            tabTop.classList.add('active');
                        }
                    }
                });
    
            });
        }
    });
}

// slider keys 

if ($slidersJsItems) {
    setInterval((e) => {
        $slidersJsItems.forEach(item => {

            $slidersBodyJs.forEach(elem => {
              
                if (item.classList.contains('swiper-slide-active')) {
                    elem.classList.remove('active');
                    if (item.dataset.nums === elem.dataset.nums) {
                        
                        elem.classList.add('active');
                        document.body.classList = '';
                        $modalMenu.classList.remove('case1');
                        $modalMenu.classList.remove('case2');
                        $modalMenu.classList.remove('case3');
                        $modalMenu.classList.remove('case4');
                        $modalMenu.classList.remove('case5');

                        $modalMenu.querySelector('.gear_1').style.display = 'none';
                        $modalMenu.querySelector('.gear_2').style.display = 'none';

                        if (elem.dataset.nums == 'n1') {
                            document.body.classList.add('case1');
                            $modalMenu.classList.add('case1');
                        }
                        if (elem.dataset.nums == 'n2') {
                            document.body.classList.add('case2');
                            $modalMenu.classList.add('case2');
                        }
                        if (elem.dataset.nums == 'n3') {
                            document.body.classList.add('case3');
                            $modalMenu.classList.add('case3');
                        }
                        if (elem.dataset.nums == 'n4') {
                            document.body.classList.add('case4');
                            $modalMenu.classList.add('case4');
                        }
                        if (elem.dataset.nums == 'n5') {
                            document.body.classList.add('case5');
                            $modalMenu.classList.add('case5');
                        }
                    }
                
                }

            });
        });
    }, 1000);
}

// parallax элементы

if ($imgParallax2 && $imgParallax1) {
    window.addEventListener('mousemove', (e) => {
        $imgParallax2.style.transform = `translateX(${-e.clientX / 25}px) translateY(${-e.clientY / 25}px)`;
        // $borderLike.style.transform = `translateX(${-e.clientX / 25}px) translateY(${-e.clientY / 25}px)`;
        $imgParallax1.style.transform = `translateX(${e.clientX / 35}px) translateY(${e.clientY / 15}px)`;
        $virusJs.style.transform = `translateX(${e.clientX / 35}px) translateY(${e.clientY / 15}px)`;

    });
}

// подьем шестеренок

document.addEventListener('DOMContentLoaded', () => {
    $gearJs.forEach(item => {
 
        setTimeout(() => {
            item.classList.remove('imgDown');
        }, 750);
        setTimeout(() => {
        
            item.classList.add('imgRotate');
        }, 900);
       
    });
    if ($linePerf) {
        $linePerf.classList.add('activeAnim');
    }

  
})

// появление элемента при скролле
if (sectionDwn) {
    window.addEventListener('scroll', (e) => {

        const scrlAll = document.querySelectorAll('.scrl-all');
      
        scrlAll.forEach(item => {
            let targetPos = {
                top: window.pageYOffset + item.getBoundingClientRect().top,
                left: window.pageXOffset + item.getBoundingClientRect().left,
                right: window.pageXOffset + item.getBoundingClientRect().right,
                bottom: window.pageYOffset + item.getBoundingClientRect().bottom,
              };
              let windowsPos = {
                top: window.pageYOffset,
                left: window.pageXOffset,
                right: window.pageXOffset + document.documentElement.clientWidth,
                bottom: window.pageYOffset + document.documentElement.clientHeight,
              };
            
              if (targetPos.bottom > windowsPos.top && 
                targetPos.top < windowsPos.bottom && 
                targetPos.right > windowsPos.left &&
                targetPos.left < windowsPos.right) {
                  setTimeout(() => {
                    item.style.opacity = '1';
                    item.classList.add('active');
                  }, 500);
                  
                 
                } else {
                    item.style.opacity = '0';
                    item.classList.remove('active');
                 
              }
        });

    
      
    });
}

// показ заблюренных блоков в кейсах

if ($rowBlocksFlexJs) {
    const blocks = [...$rowBlocksFlexJs.querySelectorAll('.row-blocks-flex-js .block')];

    $rowBlocksFlexJs.addEventListener('mousemove', () => {

        blocks.forEach(item => {
            item.classList.remove('blur');
            item.addEventListener('mousemove', () => {
                setTimeout(() => {
                    item.classList.add('blur');
                }, 1);
              
            });
        });

    });
    $rowBlocksFlexJs.addEventListener('mouseleave', () => {
        setTimeout(() => {
            blocks.forEach(item => {
                item.classList.remove('blur');
            });
            blocks[0].classList.add('blur');

        }, 5);
     
    });
  
}

if ($blockAdvantages) {
    
    const blocksPlatform = [...$blockAdvantages.querySelectorAll('.block-advantages a')];

    $blockAdvantages.addEventListener('mousemove', () => {

        blocksPlatform.forEach(item => {
            item.classList.remove('active');
            item.addEventListener('mousemove', () => {
                setTimeout(() => {
                    item.classList.add('active');
                }, 1);
              
            });
        });
    });
    $blockAdvantages.addEventListener('mouseleave', () => {
        setTimeout(() => {

            blocksPlatform.forEach(item => {
                item.classList.remove('active');
            });
            blocksPlatform[0].classList.add('active');
        }, 5);
     
    });
  
}

if ($rowImgsJs) {
    const blocksIt = [...$rowImgsJs.querySelectorAll('.row-imgs-js .block-imgs')];

    $rowImgsJs.addEventListener('mousemove', () => {

        blocksIt.forEach(item => {
            item.classList.remove('blur');
            item.addEventListener('mousemove', () => {
                setTimeout(() => {
                    item.classList.add('blur');
                }, 1);
              
            });
        });
    });
    $rowImgsJs.addEventListener('mouseleave', () => {
        setTimeout(() => {
            blocksIt.forEach(item => {
                item.classList.remove('blur');

            });
            blocksIt[0].classList.add('blur');
        }, 5);
     
    });
  
}


//  старт видео кейсах

if ($playJsVideo) {
    $playJsVideo.addEventListener('click', () => {
        $vieoStart.classList.add('active');
        $vieoStart.play();
        $playJsVideo.style.display = 'none';
    });
}


// эффект печатания текста 
const $writeText = document.querySelector('.write-text p');

if ($writeText) {
    var typed = new Typed($writeText, {
        strings: ['Cкорее бы уже с вами поработать!', 'Cкорее бы уже с вами поработать!'],
        typeSpeed: 150,   
        loop: true,
        loopCount: Infinity,
    });

}


let swiper;

if (ntstart) {
    swiper = new Swiper('.swiper-container', {

        spaceBetween: 0,
        loop: false,
        centeredSlides: true,
        slidesPerView: 1,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
} else {
    swiper = new Swiper('.swiper-container', {
        initialSlide: 3,
        spaceBetween: 0,
        loop: false,
        centeredSlides: true,
        slidesPerView: 1,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
}


